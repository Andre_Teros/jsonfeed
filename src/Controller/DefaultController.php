<?php
declare(strict_types=1);

namespace App\Controller;

use App\Service\FeedTransformer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\HeaderUtils;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

class DefaultController extends AbstractController
{
    private $tokenManager;

    public function __construct(CsrfTokenManagerInterface $tokenManager)
    {
        $this->tokenManager = $tokenManager;
    }

    public function index(): Response
    {
        return $this->render('index.html.twig', [
            'availableColumns' => FeedTransformer::$availableColumns,
        ]);
    }

    public function csvFile(Request $request, FeedTransformer $feedTransformer): Response
    {
        $columns = $request->request->get('columns');

        if (!$this->csrfValid($request->request->get('_csrf_token')) || !$this->checkColumns($columns)) {
            return $this->redirectToRoute('index');
        }

        $rawCsv = $feedTransformer->getCsv($columns);

        $response = new Response($rawCsv);

        $disposition = HeaderUtils::makeDisposition(
            HeaderUtils::DISPOSITION_ATTACHMENT,
            'feed.csv'
        );

        $response->headers->set('Content-Disposition', $disposition);
        $response->headers->set('Content-Type', 'text/csv');

        return $response;
    }

    private function csrfValid($value): bool
    {
        if (!$this->tokenManager->isTokenValid(new CsrfToken('form', $value))) {
            $this->addFlash('error', 'Invalid csrf token');

            return false;
        }

        return true;
    }

    private function checkColumns($columns): bool
    {
        if (empty($columns)) {
            $this->addFlash('error', 'Select at least one column');

            return false;
        }

        $unknownColumns = array_diff($columns, FeedTransformer::$availableColumns);
        if (!empty($unknownColumns)) {
            $this->addFlash('error', 'Unknown columns: [' . implode(', ', $unknownColumns) . ']');

            return false;
        }

        return true;
    }
}
