<?php
declare(strict_types=1);

namespace App\Dto;

class Petition
{
    public $title;
    public $link;
    public $description;
    public $petitionID;
    public $signature_count;
    public $summary;
    public $stopdate;
}
