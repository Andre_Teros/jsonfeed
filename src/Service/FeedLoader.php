<?php
declare(strict_types=1);

namespace App\Service;

class FeedLoader
{
    private $url;

    public function __construct(string $url)
    {
        $this->url = $url;
    }

    public function getFeed(): string
    {
        return file_get_contents($this->url);
    }
}
