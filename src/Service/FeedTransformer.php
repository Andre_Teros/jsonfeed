<?php
declare(strict_types=1);

namespace App\Service;

use App\Dto\Petition;
use Symfony\Component\Serializer\Encoder\DecoderInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\SerializerInterface;

class FeedTransformer
{
    private $feedLoader;
    private $decoder;
    private $denormalizer;
    private $serializer;

    public static $availableColumns = [
        'title',
        'link',
        'description',
        'petitionID',
        'signature_count',
        'summary',
    ];

    public function __construct(
        FeedLoader $feedLoader,
        DecoderInterface $decoder,
        DenormalizerInterface $denormalizer,
        SerializerInterface $serializer
    ) {
        $this->feedLoader = $feedLoader;
        $this->decoder = $decoder;
        $this->denormalizer = $denormalizer;
        $this->serializer = $serializer;
    }

    public function getCsv(array $columns): string
    {
        $jsonFeed = $this->feedLoader->getFeed();
        $aFeed = $this->decoder->decode($jsonFeed, 'json');

        $petitions = $this->denormalizer->denormalize($aFeed['petitions'], 'App\Dto\Petition[]');

        usort($petitions, [$this, 'sortByDate']);

        return $this->serializer->serialize($petitions, 'csv', ['attributes' => $columns]);
    }

    private function sortByDate(Petition $petitionA, Petition $petitionB): int
    {
        $dateTimeA = \DateTime::createFromFormat('Y-m-d', $petitionA->stopdate);
        $dateTimeB = \DateTime::createFromFormat('Y-m-d', $petitionB->stopdate);

        return -($dateTimeA->getTimestamp() <=> $dateTimeB->getTimestamp());
    }
}
